import {BrowserRouter, Routes, Route} from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import Login from "./components/Login";
import BrgTersedia from "./pages/BrgTersedia"
import AddBarangTersedia from "./pages/AddBarangTersedia";
import EditBarangTersedia from "./pages/EditBarangTersedia";
import BrgRusak from "./pages/BrgRusak";
import AddBarangRusak from "./pages/AddBarangRusak";
import EditBarangRusak from "./pages/EditBarangRusak";
import KategoriBrg from "./pages/KategoriBrg";
import AddKategoriBarang from "./pages/AddKategoriBarang";
import EditKategoriBarang from "./pages/EditKategoriBarang";
import LaporanBrg from "./pages/LaporanBrg";
import DataPga from "./pages/DataPga";
import AddDataPengguna from "./pages/AddDataPengguna";
import EditDataPengguna from "./pages/EditDataPengguna";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/brgtersedia" element={<BrgTersedia />} />
          <Route path="/brgtersedia/add" element={<AddBarangTersedia />} />
          <Route path="/brgtersedia/edit/:id" element={<EditBarangTersedia />} />
          <Route path="/brgrusak" element={<BrgRusak />} />
          <Route path="/brgrusak/add" element={<AddBarangRusak />} />
          <Route path="/brgrusak/edit/:id" element={<EditBarangRusak />} />
          <Route path="/kategoribrg" element={<KategoriBrg />} />
          <Route path="/kategoribrg/add" element={<AddKategoriBarang />} />
          <Route path="/kategoribrg/edit/:id" element={<EditKategoriBarang />} />
          <Route path="/laporanbrg" element={<LaporanBrg />} />
          <Route path="/datapga" element={<DataPga />} />
          <Route path="/datapga/add" element={<AddDataPengguna />} />
          <Route path="/datapga/edit/:id" element={<EditDataPengguna />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
