import React from 'react'

const BarangRusak = () => {
  return (
    <div>
        <h1 className='title'>Barang Rusak</h1>
        <h2 className='subtitile'>Daftar Barang Rusak</h2>
        <table className='table is-striped is-fullwidth'>
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Kode Barang</th>
                    <th>Tgl Ditambahkan</th>
                    <th>Nama Barang</th>
                    <th>Warna</th>
                    <th>Kategori</th>
                    <th>Jumlah / Satuan</th>
                    <th>Keterangan</th>
                    <th>Barcode</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
  )
}

export default BarangRusak