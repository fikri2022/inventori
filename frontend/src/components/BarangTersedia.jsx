import React from 'react'

const BarangTersedia = () => {
  return (
    <div>
        <h1 className='title'>Products</h1>
        <h2 className='subtitile'>List of Products</h2>
        <table className='table is-striped is-fullwidth'>
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Kode Barang</th>
                    <th>Tgl Ditambahkan</th>
                    <th>Nama Barang</th>
                    <th>Warna</th>
                    <th>Kategori</th>
                    <th>Jumlah / Satuan</th>
                    <th>Barcode</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
  )
}

export default BarangTersedia