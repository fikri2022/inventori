import React from 'react'

const DataPengguna = () => {
  return (
    <div>
        <h1 className='title'>Data Pengguna</h1>
        <h2 className='subtitile'>Daftar Data Pengguna</h2>
        <table className='table is-striped is-fullwidth'>
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
  )
}

export default DataPengguna