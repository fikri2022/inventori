import React from 'react'

const FormAddKategoriBarang = () => {
  return (
    <div>
        <h1 className='title'>Kategori Barang</h1>
        <h2 className='subtitle'>Tambah Kategori Barang</h2>
        <div className="card is-shadowless">
            <card-content>
                <div className="content">
                <form>
                    <div className="field">
                        <label className="label">Kode Kategori</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Kode Barang' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Barcode</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Tgl Ditambahkan' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Aksi</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Nama Barang' />
                        </div>
                    </div>
                    
                    
                    <div className="field">
                        <div className="control">
                            <button className="button is-success">
                                Simpan
                            </button>
                        </div>
                        
                    </div>
                </form>
                </div>
            </card-content>
        </div>
    </div>
  )
}

export default FormAddKategoriBarang