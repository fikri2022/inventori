import React from 'react'

const FormEditBarangRusak = () => {
  return (
    <div>
        <h1 className='title'>Barang Rusak</h1>
        <h2 className='subtitle'>Edit Barang Rusak</h2>
        <div className="card is-shadowless">
            <card-content>
                <div className="content">
                <form>
                    <div className="field">
                        <label className="label">Kode Barang</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Kode Barang' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Tgl Ditambahkan</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Tgl Ditambahkan' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Nama Barang</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Nama Barang' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Warna</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Warna' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Kategori</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Kategori' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">JUmlah / Satuan</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Jumlah / Satuan' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Keterangan</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Keterangan' />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Barcode</label>
                        <div className="control">
                            <input type="text" className="input" placeholder='Barcode' />
                        </div>
                    </div>
                    
                    
                    <div className="field">
                        <div className="control">
                            <button className="button is-success">
                                Edit
                            </button>
                        </div>
                        
                    </div>
                </form>
                </div>
            </card-content>
        </div>
    </div>
  )
}

export default FormEditBarangRusak