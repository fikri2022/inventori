import React from 'react'

const KategoriBarang = () => {
  return (
    <div>
        <h1 className='title'>Kategori Barang</h1>
        <h2 className='subtitile'>Kategori Barang</h2>
        <table className='table is-striped is-fullwidth'>
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Kode Kategori</th>
                    <th>Nama Kategori</th>
                    <th>Barcode</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
  )
}

export default KategoriBarang