import React from 'react'

const LaporanBarang = () => {
  return (
    <div>
        <h1 className='title'>Laporan</h1>
        <div className="card is-shadowless">
            <card-content>
                <div className="content">
                <form>
                    <div className="field">
                        <label className="label">Pilih Laporan</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select>
                                    <option value="admin">Barang Tersedia</option>
                                    <option value="user">Barang Rusak</option>
                                    <option value="user">Kategori Barang</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="field ">
                        <label className="label">Pilih Estimasi Waktu</label>
                        <div className="control flex text-white">
                            <div className="select is-fullwidth flex-1 pr-4">
                                <select>
                                    <option value="admin">Januari 2023</option>
                                    <option value="user">Februari 2023</option>
                                    <option value="user">Maret 2023</option>
                                </select>
                            </div>
                            <div className="select is-fullwidth flex-1">
                                <select>
                                    <option value="admin">April 2023</option>
                                    <option value="user">Mei 2023</option>
                                    <option value="user">Juni 2023</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <div className="control">
                            <button className="button is-success">
                                Simpan
                            </button>
                        </div>
                        
                    </div>
                </form>
                </div>
            </card-content>
        </div>
    </div>
  )
}

export default LaporanBarang