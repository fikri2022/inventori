import React from "react";
import { NavLink } from "react-router-dom";

const Sidebar = () => {
  return (
    <div>
      <aside className="menu pl-2 has-shadow">
        <p className="menu-label">General</p>
        <ul className="menu-list">
          <li>
            <NavLink to={"/dashboard"}>Dashboard</NavLink>
          </li>
          <li>
            <NavLink to={"/brgtersedia"}>Barang Tersedia</NavLink>
          </li>
          <li>
            <NavLink to={"/brgrusak"}>Barang Rusak</NavLink>
          </li>
          <li>
            <NavLink to={"/kategoribrg"}>Kategori Barang</NavLink>
          </li>
          <li>
            <NavLink to={"/laporanbrg"}>Laporan</NavLink>
          </li>
        </ul>
        <p className="menu-label">Admin</p>
        <ul className="menu-list">
          <li>
            <NavLink to={"/datapga"}>Data Pengguna</NavLink>
          </li>
          
        </ul>
        <p className="menu-label">Settings</p>
        <ul className="menu-list">
          <li>
            <button className="button is-white">Logout</button>
          </li>
          {/* <li>
            <a>Transfers</a>
          </li>
          <li>
            <a>Balance</a>
          </li> */}
        </ul>
      </aside>
    </div>
  );
};

export default Sidebar;
