import React from 'react'

const Welcome = () => {
  return (
    <div>
        <h1 className='title'>Dashboard</h1>
        <h2 className='subtitle'>Welcome Back ...</h2>
        

        <div className="flex text-white">
          <div className="flex-1 rounded bg-cyan-800 box-border h-32 w-80 p-4 m-4 mt-10 cursor-pointer">
            {/* <MdEventAvailable size={25} /> */}
            <span className="text-lg">
              <p className="pt-2">Barang</p>
              <p className="pl-10">Tersedia</p>
            </span>
          </div>
          <div className="flex-1 rounded bg-red-800 box-border h-32 w-80 p-4 m-4 mt-10 cursor-pointer">
            {/* <TbArrowsCross size={25} /> */}
            <span className="text-lg">
              <p className="pt-2">Barang</p>
              <p className="pl-10">Rusak</p>
            </span>
          </div>
          <div className="flex-1 rounded bg-stone-700 box-border h-32 w-80 p-4 m-4 mt-10 cursor-pointer">
            {/* <VscChecklist size={25} /> */}
            <span className="text-lg">
              <p className="pt-2">Barang</p>
              <p className="pl-10">Kategori</p>
            </span>
          </div>
        </div>
    </div>
  )
}

export default Welcome