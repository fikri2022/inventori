import React from 'react'
import Layout from './Layout'
import FormAddBarangRusak from '../components/FormAddBarangRusak'

const AddBarangRusak = () => {
  return (
    <Layout>
        <FormAddBarangRusak />
    </Layout>
  )
}

export default AddBarangRusak