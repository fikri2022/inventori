import React from 'react'
import Layout from './Layout'
import FormAddBarangTersedia from '../components/FormAddBarangTersedia'

const AddBarangTersedia = () => {
  return (
    <Layout>
        <FormAddBarangTersedia />
    </Layout>
  )
}

export default AddBarangTersedia