import React from 'react'
import Layout from './Layout'
import FormAddDataPengguna from '../components/FormAddDataPengguna'

const AddDataPengguna = () => {
  return (
    <Layout>
        <FormAddDataPengguna />
    </Layout>
  )
}

export default AddDataPengguna