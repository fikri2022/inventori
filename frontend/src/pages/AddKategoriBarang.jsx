import React from 'react'
import Layout from './Layout'
import FormAddKategoriBarang from '../components/FormAddKategoriBarang'

const AddKategoriBarang = () => {
  return (
    <Layout>
        <FormAddKategoriBarang />
    </Layout>
  )
}

export default AddKategoriBarang