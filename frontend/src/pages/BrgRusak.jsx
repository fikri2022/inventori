import React from 'react'
import Layout from './Layout'
import BarangRusak from '../components/BarangRusak'

const BrgRusak = () => {
  return (
    <Layout>
        <BarangRusak />
    </Layout>
  )
}

export default BrgRusak