import React from 'react'
import Layout from './Layout'
import BarangTersedia from '../components/BarangTersedia'

const BrgTersedia = () => {
  return (
    <Layout>
        <BarangTersedia />
    </Layout>
  )
}

export default BrgTersedia