import React from 'react'
import Layout from './Layout'
import FormEditBarangRusak from '../components/FormEditBarangRusak'

const EditBarangTersedia = () => {
  return (
    <Layout>
        <FormEditBarangRusak />
    </Layout>
  )
}

export default EditBarangTersedia