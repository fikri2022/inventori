import React from 'react'
import Layout from './Layout'
import FormEditBarangTersedia from '../components/FormEditBarangTersedia'

const EditBarangTersedia = () => {
  return (
    <Layout>
        <FormEditBarangTersedia />
    </Layout>
  )
}

export default EditBarangTersedia