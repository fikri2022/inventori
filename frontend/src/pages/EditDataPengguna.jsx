import React from 'react'
import Layout from './Layout'
import FormEditDataPengguna from '../components/FormEditDataPengguna'

const EditDataPengguna = () => {
  return (
    <Layout>
        <FormEditDataPengguna />
    </Layout>
  )
}

export default EditDataPengguna