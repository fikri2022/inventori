import React from 'react'
import Layout from './Layout'
import FormEditKategoriBarang from '../components/FormEditKategoriBarang'

const EditKategoriBarang = () => {
  return (
    <Layout>
        <FormEditKategoriBarang />
    </Layout>
  )
}

export default EditKategoriBarang