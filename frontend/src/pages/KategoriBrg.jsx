import React from 'react'
import Layout from './Layout'
import KategoriBarang from '../components/KategoriBarang'

const KategoriBrg = () => {
  return (
    <Layout>
        <KategoriBarang />
    </Layout>
  )
}

export default KategoriBrg