/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  theme: {
    colors: {
      'WhiteF'  :'#FFFFFF',
      'BlueF'   :'#225560',
      'BgF'   :'#F8F9FC'
    },
    extend: {},
  },
  plugins: [],
}
